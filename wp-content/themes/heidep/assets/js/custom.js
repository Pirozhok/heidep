/**
 * Created by Alexey on 31.03.2017.
 */
$(document).ready(function(){

    alert('all right');

    $("input[name=phone]").mask("7 (999) 999-99-99");

/*    $('.main-slides').bxSlider({
        speed: 800,
        auto: true,
        nextSelector: '.main-slider__next',
        prevSelector: '.main-slider__prev',
        nextText: ' ',
        prevText: ' ',
        onSliderLoad: function () {
            $(".main-slider .bx-pager.bx-default-pager").remove();
        }
    });*/

    var prodSliders = $('.product-slider');
    // for(var i = 0; i < prodSliders.length; i++) {
    //     prodSliders.eq(i).find('.product-slides').bxSlider({
    //         pagerCustom: prodSliders.eq(i).find('.product-slider-control'),
    //         nextSelector: prodSliders.eq(i).find('.product-slider__next'),
    //         prevSelector: prodSliders.eq(i).find('.product-slider__prev'),
    //         nextText: ' ',
    //         prevText: ' ',
    //         onSliderLoad: function () {
    //             $(".product-slider .bx-pager.bx-default-pager").remove();
    //         }
    //     });
    // }

    $('.select-cities, .select').on('click', function(e){
        e.stopPropagation();
        $('body').off('click');
        var self = this;

        if($(this).hasClass('open')) {
            $(this).removeClass('open').find('.select-cities-list, .select-list').slideUp();
        } else {
            $(this).addClass('open').find('.select-cities-list, .select-list').slideDown();
            $('body').on('click', function(){
                $(self).removeClass('open').find('.select-cities-list, .select-list').slideUp();
            });
        }
    });

    $('.select-cities_job .select-cities-list__item').on('click', function(){
        $(this)
            .parents('.select-cities_job')
            .find('.select-cities__selected')
            .text($(this).text());

        //delete this
        $('.vacancies').css('display', 'none');
        $('.vacancies_none').css('display', 'block');
        //end delete this
    });

    $('.select-list__item').on('click', function(){
        $(this)
            .parents('.select')
            .addClass('active')
            .find('.select__select')
            .text($(this).text());

        $(this)
            .parents('.select')
            .find('input')
            .val($(this).text());
    });

    $('.menu-btn, .menu-wr__close').on('click', function(e){
        e.stopPropagation();
        $('.menu-wr').toggleClass('open');
    });

    $('form').submit(function(e){
        e.preventDefault();
        var email = $(this).find('input[name=email]');
        if(email.index() != -1) {
            var english = /^[A-Za-z0-9@-\\.]+[@][A-Za-z0-9-]+[.][A-Za-z0-9@-]+$/;
            if(!english.test(email.val())){
                email.addClass('err');
                return;
            } else {
                email.removeClass('err');
            }
        }

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: $(this).serialize(),
            success: function (data, textStatus, jqXHR) {
            }
        });
    });

    if($(window).width() <= 500) {
        var tetxField = $('.f-b-form__field_text');
        tetxField.focusin(function(){
            $(this).addClass('fix');
        });

        tetxField.focusout(function(){
            if(!$(this).val()) {
                $(this).removeClass('fix');
            }
        });
    }

    var productHeaderBlock = $('.p-header-block__bd');
    if(productHeaderBlock.index() != -1) {
        setTimeout(function(){
            productHeaderBlock.addClass('animate');
        }, 500);
    }

    //calculator
    if($('.calculator').index() != -1) {
        var workPlaces = document.getElementById('workPlaces'),
            clientsCount = document.getElementById('clientsCount'),
            averagePrice = document.getElementById('averagePrice'),
            placesResult = $('.calculator-item_places .calculator-item__result'),
            clientsResult = $('.calculator-item_clients .calculator-item__result'),
            priceResult = $('.calculator-item_price .calculator-item__result');

        noUiSlider.create(workPlaces, {
            start: [2],
            connect: [true, false],
            step: 1,
            range: {
                'min': [2],
                'max': [6]
            }
        });

        noUiSlider.create(clientsCount, {
            start: [20],
            connect: [true, false],
            step: 1,
            range: {
                'min': [20],
                'max': [60]
            }
        });

        noUiSlider.create(averagePrice, {
            start: [1000],
            connect: [true, false],
            step: 1,
            range: {
                'min': [1000],
                'max': [1500]
            }
        });

        function setCalcResult(){
            var resultElem = $('.calculator-result__sum i'), value = 0, str;

            value = (+clientsResult.text() * +priceResult.text() * 30) + '';
            str = value;
            value = '';
            while(str.length > 3) {
                value = str.slice(-3) + ' ' + value;
                str = str.slice(0, -3);
            }
            value = str + ' ' + value;
            resultElem.text(value);
        }

        var setValue = false;

        workPlaces.noUiSlider.on('update', function( values, handle ) {
            placesResult.text((values[handle] + '').replace('.00', '') );
            if(!setValue) {
                clientsCount.noUiSlider.set(values[handle] * 10);
            }

            setCalcResult();
        });

        clientsCount.noUiSlider.on('update', function( values, handle ) {
            clientsResult.text((values[handle] + '').replace('.00', ''));
            setValue = true;
            if(setValue) {
                workPlaces.noUiSlider.set(Math.ceil(values[handle] / 10));
            }

            setValue = false;
            setCalcResult();
        });

        averagePrice.noUiSlider.on('update', function( values, handle ) {
            priceResult.text((values[handle] + '').replace('.00', ''));
            setCalcResult();
        });

    }
    //end calculator

    //chart animate
    var chart = $('.chart');
    if(chart.index() != -1) {
        var chartTop = chart.offset().top - 550;
        $(window).scroll(function(){
            var scrollTop = $(window).scrollTop();
            if(scrollTop > chartTop) {
                $(chart).addClass('animate');
            }
        });
    }
    //end chart animate

    //scroll
    $('.scroll-btn').click(function(){
        var href = $(this).data('href');
        $('html,body').stop().animate({ scrollTop: $(href).offset().top }, 1000);
    });
    //end scroll

    //popup
    function addPopupEvent(selecton, popup){
        $(selecton).click(function (e) {
            e.preventDefault();


            $('.overlay').fadeIn(400,
                function () {
                    $('.page-wrap').css({'height': '100%', 'overflow-y': 'scroll'});
                    $('html').css({'overflow-y': 'hidden'});
                    $(popup).css('display', 'block');
                    var wHeight = +$(window).height() - 60, pHeight = $(popup).outerHeight();
                    if(+pHeight < wHeight) {
                        $(popup).css('top', (wHeight / 2) - (pHeight / 2));
                    }
                    $(popup).animate({opacity: 1}, 200);
                });
            return false;
        });
    }

    addPopupEvent('.service', '.popup-service');
    addPopupEvent('.calculator-result__btn', '.popup-fb');

    $('.popup').on('click', function(e) {
        e.stopPropagation();
    });

    $('.popup__close, .overlay').click(function () {
        $('.popup')
            .animate({opacity: 0}, 200,
                function () {
                    $('.page-wrap').css({'height': 'auto', 'overflow-y': 'hidden'});
                    $('html').css({'overflow-y': 'auto'});
                    $(this).css('display', 'none');
                    $('.overlay').fadeOut(400);
                }
            );
    });
    //end popup
});
