<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

    <header class="header content spacer">
        <a href="#" class="logo">
            <img src="images/logo.png" alt="Логотип" class="logo__icon">
            <span class="logo__desc">Стрижка, бритье и другие услуги</span>
        </a>

        <div class="h-center">
            <div class="spacer">
                <div class="select-cities-wr">
                    <span class="select-cities-wr__desc">
                        <i class="desktop-text">Выберите город:</i>
                        <i class="mobile-text">Город:</i>
                    </span>

                    <div class="select-cities">
                        <i class="icon icon-select-arrow"></i>
                        <p class="select-cities__selected">Выберите город</p>

                        <div class="select-cities-list" style="display: none;">
                            <a class="select-cities-list__item" href="#">
                                <span>Калининград</span>
                            </a>

                            <a class="select-cities-list__item" href="#">
                                <span>Москва</span>
                            </a>

                            <a class="select-cities-list__item" href="#">
                                <span>Сочи</span>
                            </a>

                            <a class="select-cities-list__item" href="#">
                                <span>Санкт-Петербург</span>
                            </a>

                            <a class="select-cities-list__item" href="#">
                                <span>Калининград</span>
                            </a>

                            <a class="select-cities-list__item" href="#">
                                <span>Омск</span>
                            </a>

                            <a class="select-cities-list__item" href="#">
                                <span>Ростов-на-Дону</span>
                            </a>

                            <a class="select-cities-list__item" href="#">
                                <span>Уфа</span>
                            </a>

                        </div><!--select-cities-list-->

                    </div><!--select-cities-->

                </div><!--select-cities-wr-->

                <p class="header__slogan">Остров Мужской Брутальности!</p>

            </div><!--spacer-->

            <div class="hr header-hr">
                <span class="hr-center">
                    <i class="icon icon-hr-1"></i>
                </span>
            </div><!--hr-->

            <nav class="main-nav spacer">
                <a href="#" class="main-nav__link">Услуги</a>
                <a href="#" class="main-nav__link">Косметика</a>
                <a href="#" class="main-nav__link">Блог</a>
                <a href="#" class="main-nav__link">Франшиза</a>
                <a href="#" class="main-nav__link">Карьера</a>
            </nav>

        </div><!--h-center-->

        <div class="r-header">
            <a href="https://w43854.yclients.com/" target="_blank" class="brown-btn header__btn box">
                <i>Записаться онлайн</i>
            </a>

            <span class="menu-btn">
                <span class="menu-btn__text">Меню</span>
                <i class="menu-btn__icon"></i>
            </span>

        </div><!--r-header-->

    </header>

	<?php
	// If a regular post or page, and not the front page, show the featured image.
	if ( has_post_thumbnail() && ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) ) :
		echo '<div class="single-featured-image-header">';
		the_post_thumbnail( 'twentyseventeen-featured-image' );
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>

	<div class="site-content-contain">
		<div id="content" class="site-content">
