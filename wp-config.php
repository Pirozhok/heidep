<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'heidep');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '1');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pMH&_b< li4drsmkraWruY(F7C<U,JQ==1Pxe !Oi+[BzbkYPgI(8gIB[|kuYo./');
define('SECURE_AUTH_KEY',  '0I78O.El?717s=8k!M5cC_^B4pDro5,K~B?;WumiC5$K@iBYiUuG~Um+<:[x;fOu');
define('LOGGED_IN_KEY',    'u4a!poOuUs^Sf;VPaCT*16lf4b@V#i*OcCNB~TxeKmaa:{V/J{*Iz]2<pypA9w1>');
define('NONCE_KEY',        'V7fKz]}kAF}mZQMik}%`>)fQ9#4N 8qa)^e=1Q3e^e PjsVB+cs|18w=q>Lh7 IC');
define('AUTH_SALT',        'r|GF [+c{dXW`i!jvQL$]oNcmc VFWRyVSvyZosQQ~ydU:<%j9xe>7!EDOW>A>k>');
define('SECURE_AUTH_SALT', '.pM91q<$1-*/YWsJ=N`(bO4/!Bf[FHa}KFX-x#qm> oDvqpxU@fym0{WQ*_XcG[?');
define('LOGGED_IN_SALT',   '1Iuu<~xI1}~H$zF586rQ{LTbR}#C.;I%IuPGLEe#;8AK<30apg!@%8J18r<Z$@!s');
define('NONCE_SALT',       '?C%2Z%TBY?(LI!G5}}<;Yz2)J>@Hk/9!-xp*`PQ5(9sN~Sxz(&f11,gq!0AJ>F!j');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
